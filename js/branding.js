export function brandingHover() {
    const logo = document.querySelector(".header__branding h2");
    logo.addEventListener("mousemove", e => {
        let offset = e.offsetX / 5;
        logo.style.backgroundPositionX = `-${offset}px`;
    });
};