export function deactivateToggle() {
    if (window.innerWidth > 450) {
        const menuOpen = document.querySelector(".menu__toggle--open");
        if (menuOpen) toggleMenu();
    }
}

export function toggleMenu() {
    const container = document.querySelector(".menu__container");
    const main = document.querySelector(".menu__main");
    const hiddenMobile = document.querySelectorAll(".hidden-mobile");
    const toggle = document.querySelector(".menu__toggle");

    container.classList.toggle("menu__toggle--expand");
    main.classList.toggle("menu__toggle--expand");

    hiddenMobile.forEach(el => {
        el.style.animation = "none";
        el.classList.toggle("menu__toggle__item--display");
    });

    toggle.classList.toggle("menu__toggle--open");
}