// Import modules
import {brandingHover} from "./branding";
import {toggleMenu, deactivateToggle} from "./menu";

// Start JS logic
let a = performance.now();
console.info("Loading DOM...");
document.addEventListener("DOMContentLoaded", (() => {
    let b = performance.now();
    console.info(`DOM ready in ${b - a}ms!`);

    // Interesting hover effect for branding logo
    brandingHover();

    // Functionality relating to the toggle menu
    window.toggleMenu = toggleMenu;
    window.addEventListener('resize', deactivateToggle);
}));